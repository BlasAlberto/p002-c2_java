package com.example.practica002_java;

public class ItemData {

    private String txtCategoria;
    private String txtDescripcion;
    private Integer imageId;


    public ItemData(){
        this.txtCategoria = "";
        this.txtDescripcion = "";
        this.imageId = 0;
    }
    public ItemData(String categoria, String descripcion, Integer imageId){
        this.txtCategoria = categoria;
        this.txtDescripcion = descripcion;
        this.imageId = imageId;
    }


    public String getTxtCategoria() {
        return txtCategoria;
    }
    public void setTxtCategoria(String txtCategoria) {
        this.txtCategoria = txtCategoria;
    }
    public String getTxtDescripcion() {
        return txtDescripcion;
    }
    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }
    public Integer getImageId() {
        return imageId;
    }
    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}
