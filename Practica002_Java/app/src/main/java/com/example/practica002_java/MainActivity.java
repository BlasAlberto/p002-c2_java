package com.example.practica002_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.itemFrappses), getString(R.string.msgFrapsses).toString(), R.drawable.categorias));
        list.add(new ItemData(getString(R.string.itemAgradecimiento), getString(R.string.msgAgradecimiento).toString(), R.drawable.agradecimiento));
        list.add(new ItemData(getString(R.string.itemAmor), getString(R.string.msgAmor).toString(), R.drawable.corazon));
        list.add(new ItemData(getString(R.string.itemNewyear), getString(R.string.msgNewYear).toString(), R.drawable.nuevo));
        list.add(new ItemData(getString(R.string.itemCanciones), getString(R.string.msgCanciones).toString(), R.drawable.canciones));

        sp = (ListView) findViewById(R.id.spinner1);
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spiner_layout, R.id.lblCategorias, list);
        sp.setAdapter(adapter);

        sp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(adapterView.getContext(),
                        getString(R.string.msgSeleccionado).toString() + " " + ((ItemData) adapterView.getItemAtPosition(i)).getTxtCategoria(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}