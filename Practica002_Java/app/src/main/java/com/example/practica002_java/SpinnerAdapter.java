package com.example.practica002_java;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<ItemData> {

    int groupId;
    Activity context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;

    public SpinnerAdapter(Activity context, int groupId, int id, ArrayList<ItemData> list){
        super(context, id, list);

        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int posicion, View convertView, ViewGroup parent){
        View itemView = this.inflater.inflate(groupId, parent, false);

        ImageView image = (ImageView) itemView.findViewById(R.id.imgCategoria);
        image.setImageResource(list.get(posicion).getImageId());

        TextView textCategoria = (TextView) itemView.findViewById(R.id.lblCategorias);
        textCategoria.setText(list.get(posicion).getTxtCategoria());

        TextView txtDescripcion = (TextView) itemView.findViewById(R.id.lblDescripcion);
        txtDescripcion.setText(list.get(posicion).getTxtDescripcion());

        return itemView;
    }

    public View getDropDownView(int posicion,View convertView,ViewGroup parent){
        return getView(posicion, convertView, parent);
    }

}
